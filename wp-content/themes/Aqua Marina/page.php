<?php

the_post();
get_header();
$fields = get_fields();
the_content();
?>
<?php get_footer() ?>
