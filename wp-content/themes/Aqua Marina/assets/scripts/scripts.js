(function($) {

	$( document ).ready(function() {
		// Load more items in gallery
		var sizeLi = $('#members-block .card-member-col').size();
		var x = 3;
		$('#load-more-items-members').click(function () {
			x = (x + 3 <= sizeLi) ? x + 3 : sizeLi;
			if (x === sizeLi) {
				$('#load-more-items-members').addClass('hide');
			}
			$('#members-block .card-member-col:lt('+x+')').addClass('show-col');
		});
		$(window).on('resize', function() {

			if($(window).width < 1200) {
			}
			var linc2 = $('.drop-container'),
				timeoutId;
			$('.first-sub-menu').hover(function(){
				var content = $(this).children('.sub-menu').clone();
				linc2.html(content);
				clearTimeout(timeoutId);
				linc2.show();
			}, function(){
				timeoutId = setTimeout($.proxy(linc2,'hide'), 1000);
			});
			linc2.mouseenter(function(){
				clearTimeout(timeoutId);
			}).mouseleave(function(){
				linc2.hide();
			});
		});
		$( '.process-bar-item' ).each(function() {
			var percent = $(this).data('value');
			//For too high values :
			if (percent > 100){
				percent = 100;
			}
			//$(this).css('width', percent+'%' );
			//With animation as asked :
			$(this).children('.process-line-color').animate({width: percent+'%' }, 2000);
		});
		$('.search-trigger').click(function () {
			$('.pop-search').addClass('show-search');
			$('.float-search').addClass('show-float-search');
		});
		$('.close-search').click(function () {
			$('.pop-search').removeClass('show-search');
			$('.float-search').removeClass('show-float-search');
		});
		$('.main-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: false,
			dots: true,
		});
		$('.reviews-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.gallery-slider').slick({
			slidesToShow: 6,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 3000,
			rtl: true,
			arrows: false,
			dots: false,
			responsive: [
				{
					breakpoint: 1600,
					settings: {
						slidesToShow: 5
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 650,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 450,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('.card-member-img').click(function() {
			$(this).parent('.post-item-img').children('.overlay-review-wrap').addClass('show');
		});
		$('.card-member-close').click(function() {
			$(this).parent('.overlay-review-item').parent('.overlay-review-wrap').removeClass('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').children('.arrow-top').addClass('arrow-bottom');
			show.parent().children('.question-title').addClass('active-faq');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').children('.arrow-top').removeClass('arrow-bottom');
			collapsed.parent().children('.question-title').removeClass('active-faq');
		});
	});
	$('.load-more-posts').click(function() {
		var termID = $(this).data('term');
		var params = $('.take-json').html();
		// var postType = $(this).data('type');
		// var taxType = $(this).data('tax-type');

		var ids = '';
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				// postType: postType,
				termID: termID,
				ids: ids,
				// taxType: taxType,
				params: params,
				action: 'get_more_function',
			},
			success: function (data) {
				if (!data.html) {
					$('.load-more-posts').addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});

	// $('#load-more-items-members').click(function() {
	// 	var pageId = $('.page-id').data('id');
	// 	var ids = '';
	// 	$('.card-member-col').each(function(i, obj) {
	// 		ids += $(obj).data('id') + ',';
	// 	});
	// 	jQuery.ajax({
	// 		url: '/wp-admin/admin-ajax.php',
	// 		dataType: 'json',
	// 		data: {
	// 			ids: ids,
	// 			pageId : pageId,
	// 			// taxType: taxType,
	// 			action: 'get_more_members_function',
	// 		},
	// 		success: function (data) {
	// 			if (!data.html) {
	// 				$('.load-more-link').addClass('hide');
	// 			}
	// 			$('.put-here-posts').append(data.html);
	// 		}
	// 	});
	// });


})( jQuery );
var v = document.getElementById('video-home');
var tr = document.getElementById('video-button');
if (tr && v) {
	tr.addEventListener(
		'play',
		function() {
			v.play();
		},
		false);

	tr.onclick = function() {
		if (v.paused) {
			v.play();
			tr.classList.add('hide');
		} else {
			v.pause();
			tr.classList.remove('hide');
		}
		return false;
	};
}
var v1 = document.getElementById('video-home-1');
var tr1 = document.getElementById('video-button-1');
if (tr1 && v1) {
	tr1.addEventListener(
		'play',
		function() {
			v1.play();
		},
		false);

	tr1.onclick = function() {
		if (v1.paused) {
			v1.play();
			tr1.classList.add('hide');
		} else {
			v1.pause();
			tr1.classList.remove('hide');
		}
		return false;
	};
}
var v2 = document.getElementById('video-home-2');
var tr2 = document.getElementById('video-button-2');
if (tr2 && v2) {
	tr2.addEventListener(
		'play',
		function() {
			v2.play();
		},
		false);

	tr2.onclick = function() {
		if (v2.paused) {
			v2.play();
			tr2.classList.add('hide');
		} else {
			v2.pause();
			tr2.classList.remove('hide');
		}
		return false;
	};
}
