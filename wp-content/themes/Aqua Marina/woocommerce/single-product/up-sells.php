<?php
/**
 * Single Product Up-Sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/up-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $upsells ) : ?>

	<section class="up-sells upsells products margin-50">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="block-title-small">בא ביחד עם המוצר</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<div class="col-xl-10 col-md-11 col-12">
					<div class="row justify-content-center align-items-start">
						<?php foreach ( $upsells as $upsell ) {
							$post_object = get_post( $upsell->get_id() );

							setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

							echo '<div class="col-lg-3 col-sm-6 col-12">';
							get_template_part('views/partials/card', 'product_upsale',
									[
											'product' => $post_object,
									]);
							echo '</div>';
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php
endif;

wp_reset_postdata();
