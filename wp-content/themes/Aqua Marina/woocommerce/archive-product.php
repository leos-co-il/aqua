<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
global $wp;
if ($wp->request === 'shop') {
	$query = wc_get_page_id('shop');
} else {
	$query_curr = get_queried_object();
	$query = $query_curr;
}
$top_img = get_field('shop_top_img', $query);
/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<div class="top-block-back" <?php if ($top_img) : ?>
	style="background-image: url('<?= $top_img['url']; ?>')"
	<?php endif; ?>></div>
<section class="woocommerce-products-header">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
					<h1 class="block-title"><?php woocommerce_page_title(); ?></h1>
				<?php endif; ?>
				<div class="base-output text-center">
					<?php
					/**
					 * Hook: woocommerce_archive_description.
					 *
					 * @hooked woocommerce_taxonomy_archive_description - 10
					 * @hooked woocommerce_product_archive_description - 10
					 */
					do_action( 'woocommerce_archive_description' );
					?>
				</div>
			</div>
		</div>
		<?php if ($terms = get_terms([
				'taxonomy' => 'product_cat',
				'hide_empty' => true,
				'parent' => 0,
		])) : ?>
			<div class="row justify-content-center">
				<div class="col-lg-10 col-sm-11 col-12">
					<div class="row align-items-stretch justify-content-between mb-5">
						<?php foreach ($terms as $x => $cat_link) : ?>
							<div class="col-md-auto col-cat-main wow fadeInUp" data-wow-delay="0.<?= $x * 2; ?>s">
								<a class="base-link bigger-link <?= (($query->term_id) === ($cat_link->term_id)) ? 'product-term-link' : ''; ?>"
								   href="<?= get_term_link($cat_link); ?>">
									<?= $cat_link->name; ?>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>
<?php if ( woocommerce_product_loop() ) {

//	/**
//	 * Hook: woocommerce_before_shop_loop.
//	 *
//	 * @hooked woocommerce_output_all_notices - 10
//	 * @hooked woocommerce_result_count - 20
//	 * @hooked woocommerce_catalog_ordering - 30
//	 */
//	do_action( 'woocommerce_before_shop_loop' );
//
//	woocommerce_product_loop_start();
	echo '<div class="container"><div class="row justify-content-center align-items-start">';
	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );
			echo '<div class="col-xl-3 col-lg-4 col-sm-6 col-12 product-card-col">';
			wc_get_template_part( 'content', 'product' );
			echo '</div>';
		}
	}
	echo '</div></div><div class="container mb-4"><div class="row justify-content-center"><div class="col-auto">';
//	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
	echo '</div></div></div>';
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );
get_template_part('views/partials/repeat', 'instagram');
if ($gallery = get_field('shop_gallery', $query)) {
	get_template_part('views/partials/content', 'gallery', [
			'gallery' => $gallery,
	]);
}
get_footer( 'shop' );
