<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$category = get_the_terms($product->get_id(), 'product_cat');
$img = $product->get_image_id();
$fields = get_fields();
if ($fields['product_main_slider']) : ?>
	<section class="main-product-slider">
		<div class="reviews-slider" dir="rtl">
			<?php foreach ($fields['product_main_slider'] as $slide) : ?>
				<div style="background-image: url('<?= $slide['url']; ?>')" class="prod-slide"></div>
			<?php endforeach; ?>
		</div>
	</section>
<?php else: ?>
<div class="post-image-top product-top"></div>
<?php endif; ?>
<div class="add-to-cart-line">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto col-add-line">
				<?php do_action('add_and_price'); ?>
			</div>
		</div>
	</div>
</div>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-4 col-lg-6 col-md-8 col-sm-10 col-12">
				<div class="page-prod-img wow fadeInUp" data-wow-delay="0.3s">
					<img src="<?= wp_get_attachment_image_url($product->get_image_id(), 'large'); ?>" alt="product-image">
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<?php foreach ($category as $cat) : if ($cat->parent !== 0) : ?>
				<div class="col-auto">
					<a class="base-link bigger-link product-term-link" href="<?= get_term_link($cat); ?>">
						חזרה לסדרה
					</a>
				</div>
			<?php endif; endforeach; ?>
		</div>
		<div class="row justify-content-center">
			<div class="col-xl-10 col-md-11 col-12 mb-5">
				<?php foreach ($category as $cat) : if ($cat->parent !== 0) : ?>
					<h2 class="home-main-title prod-cat-main-title">
						<?= 'סדרת '.$cat->name; ?>
					</h2>
					<?php if ($en = get_field('cat_en', $cat)) : ?>
						<h3 class="block-title block-en-title">
							<?= 'סדרת '.$en; ?>
						</h3>
					<?php endif;?>
				<?php endif; endforeach; ?>
				<h4 class="mid-title-center"><?= $product->get_sku(); ?></h4>
				<?php if ($fields['product_sizes']) : ?>
					<h4 class="mid-title-center sizes-title"><?= $fields['product_sizes']; ?></h4>
				<?php endif; ?>
			</div>
			<div class="col-xl-10 col-md-11 col-12 mb-5">
				<div class="row justify-content-center align-items-start">
					<?php if ($fields['process_lines']) : ?>
						<div class="col-lg-6 col-12">
							<?php foreach ($fields['process_lines'] as $item) : ?>
								<div class="process-bar-item" data-value="<?= $item['pro_percent']; ?>">
									<div class="process-line-color">
										<span class="process-bar-title">
											<?= $item['pro_percent'].'%'; ?>
										</span>
									</div>
									<span class="process-bar-title mr-3">
										<?= $item['pro_title']; ?>
									</span>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
					<div class="<?= $fields['process_lines'] ? 'col-lg-6 col-12' : 'col-12'; ?>">
						<div class="base-output">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['product_items']) : ?>
	<section class="product-review-block margin-50">
		<div class="container mb-3">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="block-title-small"><?= $fields['product_items_title'] ?? 'פרטים'; ?></h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-xl-10 col-md-11 col-12">
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($fields['product_items'] as $i => $item) : ?>
							<div class="col-lg-4 col-sm-6 col-12 mb-30">
								<div class="part-item" <?php if ($item['item_img']) : ?>
									 style="background-image: url('<?= $item['item_img']['url']; ?>')"
								<?php endif; ?>>
									<div class="part-item-overlay">
										<h4 class="part-item-title"><?= $item['item_title']; ?></h4>
										<p class="base-text"><?= $item['item_text']; ?></p>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endif;
	get_template_part('views/partials/repeat', 'offer');
	if ($fields['product_tech_img'] || $fields['product_tech_text']) : ?>
	<section class="product-review-block margin-50">
		<div class="container mb-3">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-md-11 col-12">
					<div class="row justify-content-center align-items-start">
						<?php if ($fields['product_tech_img']) : ?>
						<div class="col-lg-6 mb-lg-0 mb-4">
							<img src="<?= $fields['product_tech_img']['url']; ?>" alt="technology" class="w-100">
						</div>
						<?php endif;
						if ($fields['product_tech_text']) : ?>
							<div class="col-lg-6">
								<div class="base-output">
									<?= $fields['product_tech_text']; ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endif;
	if ($fields['product_reviews_video']) : ?>
		<section class="product-review-block margin-50">
			<div class="container mb-3">
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="block-title-small"><?= $fields['product_reviews_title'] ?? 'ממליצים'; ?></h2>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-xl-10 col-md-11 col-12">
						<div class="product-rev-video" style="background-image: url('<?= getYoutubeThumb($fields['product_reviews_video']); ?>')">
							<span class="play-button" data-video="<?= getYoutubeId($fields['product_reviews_video']); ?>">
								<img src="<?= ICONS ?>play.png" alt="play-video">
							</span>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endif; get_template_part('views/partials/repeat', 'video_modal');
	if ($fields['product_page_video']) : ?>
		<section class="video-home">
			<video id="video-home">
				<source src="<?= $fields['product_page_video']['url']; ?>" type="video/mp4">
			</video>
			<span id="video-button" class="button-home">
			<img src="<?= ICONS ?>play.png" alt="play-video">
		</span>
		</section>
	<?php endif;
	if ($fields['product_info_content']) : ?>
		<section class="product-content-block">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto">
						<div class="product-content-table-out">
							<?= $fields['product_info_content']; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endif;
	do_action('get_upsells_here');
	if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
	endif;
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	?>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
