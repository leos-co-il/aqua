<?php

the_post();
get_header();
$fields = get_fields();

?>
<article class="page-body post-body-page">
	<div class="post-image-top" <?php if (has_post_thumbnail()) : ?>
		style="background-image: url('<?= postThumb(); ?>')"
	<?php endif; ?>>
	</div>
	<div class="body-output-part">
		<?php if ($fields['post_image']) : ?>
			<div class="absolute-post-image">
				<img src="<?= $fields['post_image']['url']; ?>" alt="post-image">
			</div>
		<?php endif; ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="<?= $fields['post_image'] ? 'col-lg-8 col-12' : 'col-12'; ?>">
					<h1 class="home-main-title"><?php the_title(); ?></h1>
					<div class="base-output">
						<?php the_content();  ?>
					</div>
				</div>
				<?php if ($fields['post_image']) : ?>
					<div class="col-lg-4">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php if ($fields['post_more_products']) : ?>
	<div class="products-output margin-50">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title"><?= $fields['post_more_products_title'] ?? 'מוצרים נוספים בחברה'; ?></h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($fields['post_more_products'] as $x => $post) : ?>
					<div class="col-lg-3 col-sm-6 col-12 product-card-col">
						<?php
						$post_object = get_post( $post->ID );

						setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

						wc_get_template_part( 'content', 'product' );
						?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
	<?php endif;
	if ($fields['post_video_link'] || $fields['post_video']) :
		$video_link = $fields['post_video_link'] ?? $fields['post_video']; if ($video_link) : ?>
	<div class="product-review-block margin-50">
		<div class="container mb-3">
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="product-rev-video" style="background-image: url('<?= getYoutubeThumb($video_link); ?>')">
						<span class="play-button" data-video="<?= getYoutubeId($video_link); ?>">
							<img src="<?= ICONS ?>play.png" alt="play-video">
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; endif;?>
</article>
<?php
$g = 'post_video_link';
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
		'posts_per_page' => 4,
		'post_type' => 'post',
		'post__not_in' => array($postId),
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $post_terms,
				],
		],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts == NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'orderby' => 'rand',
			'post_type' => 'post',
			'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="posts-output margin-50">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title"><?= $fields['same_posts_title'] ?? 'קיראו איתנו עוד'; ?></h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-xl-11">
					<div class="row justify-content-center align-items-stretch put-here-posts">
						<?php foreach ($samePosts as $x => $post) {
							get_template_part('views/partials/card', 'post', [
									'post' => $post,
							]);
						} ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endif;
get_template_part('views/partials/repeat', 'video_modal');
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
if ($fields['post_video_file']) : ?>
	<section class="video-home">
		<video id="video-home">
			<source src="<?= $fields['post_video_file']['url']; ?>" type="video/mp4">
		</video>
		<span id="video-button" class="button-home">
			<img src="<?= ICONS ?>play.png" alt="play-video">
		</span>
	</section>
<?php endif;
get_footer(); ?>
