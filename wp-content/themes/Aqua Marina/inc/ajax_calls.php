<?php

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

	if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
		exit("No naughty business please");
	}

	$result['type'] = "error";
	$result['var'] = 1;

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
add_action('wp_ajax_nopriv_update_cart_count', 'update_cart_count');
add_action('wp_ajax_update_cart_count', 'update_cart_count');
function update_cart_count(){
	global $woocommerce;
	echo $woocommerce->cart->cart_contents_count;
}

//add_action( 'wp_ajax_loadmore', 'loadmore' );
//add_action( 'wp_ajax_nopriv_loadmore', 'loadmore' );
//
//function loadmore() {
//
//	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
//	$ids = explode(',', $ids_string);
//	$paged = ! empty( $_POST[ 'paged' ] ) ? $_POST[ 'paged' ] : 1;
//	$paged++;
//
//	$args = array(
//		'paged' => $paged,
//		'post_type' => 'product',
//		'post_status' => 'publish',
//		'post__not_in' => $ids,
//	);
//	$query = new WP_Query( $args );
//	$html = '';
//	$result['html'] = '';
//	foreach( $query->posts as $item) :
//		$html = load_template_part('views/partials/card', 'product', [
//			'product' => $item,
//		]);
//		$result['html'] .= $html;
//	endforeach;
//	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
//		$result = json_encode($result);
//		echo $result;
//	} else {
//		header("Location: " . $_SERVER["HTTP_REFERER"]);
//	}
//	die();
//
//}
add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function() {
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$id_term = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
//	$post_type = (isset($_REQUEST['postType'])) ? $_REQUEST['postType'] : '';
//	$tax_name = (isset($_REQUEST['taxType'])) ? $_REQUEST['taxType'] : 'category';
//	$params_string = (isset($_REQUEST['params'])) ? $_REQUEST['params'] : '';
//	$hi = json_decode($params_string, true, 4);
	$ids = explode(',', $ids_string);
	$query = new WP_Query([
		'post_type' => 'post',
		'posts_per_page' => 4,
		'post__not_in' => $ids,
		'tax_query' => $id_term ? [
			[
				'taxonomy' => 'category',
				'field' => 'term_id',
				'terms' => $id_term,
			]
		] : '',
	]);
	$html = '';
	$result['html'] = '';
	foreach ($query->posts as $item) {
		$html = load_template_part('views/partials/card', 'post', [
			'post' => $item,
		]);
		$result['html'] .= $html;
	}

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}

//add_action('wp_ajax_nopriv_get_more_members_function', 'get_more_members_function');
//add_action('wp_ajax_get_more_members_function', 'get_more_members_function');

//function get_more_members_function() {
//	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
//	$pageId = (isset($_REQUEST['pageId'])) ? $_REQUEST['pageId'] : '';
//	$fieldAll = get_field('review_item', $pageId);
//	$ids = explode(',', $ids_string);
//	$id_2 = array_pop($ids);
//	$id_current = array_pop($id_2);
//	$html = '';
//	$result['html'] = '';
//	for ($i = $id_current; $i <= ($id_current + 3); $i++) {
//		$html = load_template_part('views/partials/card', 'member', [
//			'item' => $fieldAll['review_item'][$i],
//		]);
//		$result['html'] .= $html;
//	}
//
//	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
//		$result = json_encode($result);
//		echo $result;
//	} else {
//		header("Location: " . $_SERVER["HTTP_REFERER"]);
//	}
//	die();
//}
