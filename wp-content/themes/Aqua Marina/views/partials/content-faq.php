<?php if (isset($args['faq']) && $args['faq']) : ?>
	<div class="faq">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="block-title-small"><?= (isset($args['title']) && $args['title']) ? $args['title'] : $args['text']; ?></div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-xl-10 col-md-11 col-12">
					<div id="accordion">
						<?php foreach ($args['faq'] as $num => $item) : ?>
							<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
								<div class="question-header" id="heading_<?= $num; ?>">
									<button class="btn question-title" data-toggle="collapse"
											data-target="#faqChild<?= $num; ?>"
											aria-expanded="false" aria-controls="collapseOne">
										<?= $item['faq_question']; ?>
										<img src="<?= ICONS ?>arrow-down.png" class="arrow-top">
									</button>
									<div id="faqChild<?= $num; ?>" class="collapse faq-item"
										 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
										<div class="answer-body base-small-output">
											<?= $item['faq_answer']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
