<?php if(isset($args['post']) && $args['post']) :
$link = get_the_permalink($args['post']);
$video = get_field('post_video', $args['post']->ID);
$base_img = has_post_thumbnail($args['post']) ? postThumb($args['post']) : '';
$image = $video ? getYoutubeThumb($video) : $base_img;
?>
	<div class="col-lg-3 col-md-6 col-sm-10 col-12 post-item-col">
		<div class="post-item more-card" data-id="<?= $args['post']->ID; ?>">
			<?php if ($video) : ?>
				<div class="post-item-img" <?php if ($image) : ?>
					style="background-image: url('<?= $image; ?>')" <?php endif; ?>>
					<span class="play-button" data-video="<?= getYoutubeId($video); ?>">
						<img src="<?= ICONS ?>play.png" alt="play-video">
					</span>
				</div>
			<?php else: ?>
				<a class="post-item-img" <?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')"
				<?php endif; ?> href="<?= $link; ?>">
				</a>
			<?php endif; ?>
			<div class="home-item-content">
				<div class="flex-grow-1 d-flex justify-content-center">
					<a class="post-item-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
				</div>
				<p class="base-text text-center">
					<?= text_preview($args['post']->post_content, 14); ?>
				</p>
			</div>
		</div>
	</div>
<?php endif; ?>
