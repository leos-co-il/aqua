<?php
$insta = opt('instagram');
$title_tag = opt('inst_tag');
$title_name = opt('inst_site_link');
if ($insta || $title_name || $title_tag) : ?>
	<section class="margin-50">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<div class="insta-wrap">
						<a class="insta-img" href="<?= $insta; ?>">
							<img src="<?= ICONS ?>insta.png" alt="instagram">
						</a>
						<div class="d-flex flex-column justify-content-center align-items-center">
							<?php if ($title = opt('block_inst_title')) : ?>
								<h2 class="insta-title"><?= $title; ?></h2>
							<?php endif; ?>
							<div class="row justify-content-xl-between justify-content-center align-items-center">
								<?php if ($title_tag = opt('inst_tag')) : ?>
									<div class="col-auto">
										<h3 class="insta-tag" dir="ltr"><?= $title_tag; ?></h3>
									</div>
								<?php endif;
								if ($title_name = opt('inst_site_link')) : ?>
									<div class="col-auto">
										<h3 class="insta-name" dir="ltr"><?= $title_name; ?></h3>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
