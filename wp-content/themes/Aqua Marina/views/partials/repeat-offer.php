<?php $title = opt('offer_block_title');
$text = opt('offer_block_text');
if ($title || $text) : ?>
	<section class="offer-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-9 col-md-10 col-12">
					<h2 class="offer-title"><?= $title; ?></h2>
					<div class="centered-white-output">
						<?= $text; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
