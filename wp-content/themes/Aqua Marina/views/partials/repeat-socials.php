<?php
$facebook = opt('facebook');
$youtube = opt('youtube');
$instagram = opt('instagram');
$whatsapp = opt('whatsapp'); ?>
<div class="socials-line">
	<?php if ($whatsapp) : ?>
	<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="social-link-item">
		<img src="<?= ICONS ?>whatsapp.png" alt="whatsapp-icon">
	</a>
	<?php endif;
	if ($youtube) : ?>
	<a href="<?= $youtube; ?>" class="social-link-item">
		<img src="<?= ICONS ?>youtube.png" alt="youtube-icon">
	</a>
	<?php endif;
	if ($facebook) : ?>
		<a href="<?= $facebook; ?>" class="social-link-item">
			<img src="<?= ICONS ?>facebook.png" alt="facebook-icon">
		</a>
	<?php endif;
	if ($instagram) : ?>
		<a href="<?= $instagram; ?>" class="social-link-item">
			<img src="<?= ICONS ?>instagram.png" alt="instagram-icon">
		</a>
	<?php endif; ?>
</div>
