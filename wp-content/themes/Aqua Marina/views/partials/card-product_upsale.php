<?php if (isset($args['product']) && ($args['product'])) : $prod_item_id = $args['product'];
	$prod_item = wc_get_product($prod_item_id) ;
	$link = get_permalink($prod_item_id);
	$id = $prod_item->get_id(); ?>
	<div class="product-sale-card">
		<div class="product-sale-image"
			<?php if ($img = $prod_item->get_image_id()) : ?>
				style="background-image: url('<?= wp_get_attachment_image_url( $img, 'full' );?>')"
			<?php endif; ?>>
		</div>
		<a class="upsale-small-title" href="<?= $link; ?>">
			<?= $prod_item->get_name(); ?>
		</a>
	</div>
<?php endif; ?>
