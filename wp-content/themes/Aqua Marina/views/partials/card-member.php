<?php if(isset($args['item']) && $args['item']) : ?>
	<div class="post-item" >
		<div class="post-item-img cursor-pointer" <?php if ($args['item']['review_img']) : ?>
			style="background-image: url('<?= $args['item']['review_img']['url']; ?>')" <?php endif; ?>>
			<?php if ($args['item']['review_text']) : ?>
				<div class="card-member-img"></div>
				<div class="overlay-review-wrap">
					<div class="overlay-review-item">
						<div class="base-small-output review-text-wrapper">
							<?= $args['item']['review_text']; ?>
						</div>
						<span class="card-member-close">
							<img src="<?= ICONS ?>close-review.png" alt="close-description">
						</span>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<div class="home-item-content">
			<h4 class="rev-item-title"><?= $args['item']['review_name']; ?></h4>
			<div class="country-wrap">
				<?php if ($args['item']['review_flag']) : ?>
					<img src="<?= $args['item']['review_flag']['url']; ?>" alt="flag-icon" class="flag-icon">
				<?php endif; ?>
				<span class="base-text"><?= $args['item']['review_country']; ?></span>
			</div>
			<div class="socials-line">
				<?php if ($args['item']['rev_youtube']) : ?>
					<a href="<?= $args['item']['rev_youtube']; ?>" class="social-link">
						<img src="<?= ICONS ?>youtube.png" alt="youtube">
					</a>
				<?php endif;
				if ($args['item']['instagram']) : ?>
					<a href="<?= $args['item']['instagram']; ?>" class="social-link">
						<img src="<?= ICONS ?>instagram.png" alt="instagram">
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
