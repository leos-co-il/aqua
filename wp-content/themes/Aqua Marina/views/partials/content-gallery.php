<?php if(isset($args['gallery']) && $args['gallery']) : ?>
<div class="gallery-block-part">
	<?php foreach ($args['gallery'] as $img) : ?>
		<div class="gallery-item-base">
			<a href="<?= $img['url']; ?>" data-lightbox="gallery-line" class="gallery-item-img"
			   style="background-image: url('<?= $img['url']; ?>')">
				<img src="<?= ICONS ?>view-more.png" alt="view-more" class="cursor-pointer">
			</a>
		</div>
	<?php endforeach; ?>
</div>
<?php endif; ?>
