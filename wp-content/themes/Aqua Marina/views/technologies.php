<?php
/*
Template Name: טכנולוגיות
*/

get_header();
$fields = get_fields();
?>
<article class="page-body">
	<?php if (has_post_thumbnail()) : ?>
		<img src="<?= postThumb(); ?>" alt="page-image" class="w-100">
	<?php endif; ?>
	<div class="tech-body">
		<div class="container">
			<?php if ($fields['tech_year']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="block-title year-title"><?= $fields['tech_year']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<?php if ($fields['tech_main_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto base-output">
						<h1><?= $fields['tech_main_title']; ?></h1>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-xl-10 col-md-11 col-12">
					<div class="row justify-content-start">
						<div class="col-xl-10 col-12">
							<div class="base-output block-text">
								<?php the_content();  ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
if ($fields['tech_video_file_1']) : ?>
	<section class="video-home">
		<video id="video-home-1">
			<source src="<?= $fields['tech_video_file_1']['url']; ?>" type="video/mp4">
		</video>
		<span id="video-button-1" class="button-home">
			<img src="<?= ICONS ?>play.png" alt="play-video">
		</span>
	</section>
<?php endif;
if ($fields['page_tech_img'] || $fields['page_tech_text']) : ?>
	<section class="product-review-block margin-50">
		<div class="container mb-3">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-md-11 col-12">
					<div class="row justify-content-center align-items-start">
						<?php if ($fields['page_tech_img']) : ?>
							<div class="col-lg-6 mb-lg-0 mb-4">
								<img src="<?= $fields['page_tech_img']['url']; ?>" alt="technology" class="w-100">
							</div>
						<?php endif;
						if ($fields['page_tech_text']) : ?>
							<div class="col-lg-6">
								<div class="base-output">
									<?= $fields['page_tech_text']; ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['tech_video_file_2']) : ?>
	<section class="video-home">
		<video id="video-home-2">
			<source src="<?= $fields['tech_video_file_2']['url']; ?>" type="video/mp4">
		</video>
		<span id="video-button-2" class="button-home">
			<img src="<?= ICONS ?>play.png" alt="play-video">
		</span>
	</section>
<?php endif; ?>
<section class="block-tech-section">
	<?php if ($fields['block_img_1']) : ?>
		<div class="page-serfer-img page-serf-1">
			<img src="<?= $fields['block_img_1']['url']; ?>" alt="serfer-1">
		</div>
	<?php endif;
	if ($fields['block_img_2']) : ?>
		<div class="page-serfer-img page-serf-2">
			<img src="<?= $fields['block_img_2']['url']; ?>" alt="serfer-2">
		</div>
	<?php endif; ?>
	<div class="tech-content-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-md-11 col-12">
					<div class="row justify-content-center">
						<div class="col-auto">
							<?php if ($fields['small_tech_title']) : ?>
								<h3 class="little-block-title">
									<?= $fields['small_tech_title']; ?>
								</h3>
							<?php endif;
							if ($fields['big_tech_title']) : ?>
								<h2 class="block-title"><?= $fields['big_tech_title']; ?></h2>
							<?php endif; ?>
						</div>
					</div>
					<?php if ($fields['tech_items']) : ?>
						<div class="row justify-content-center align-items-stretch">
							<?php foreach ($fields['tech_items'] as $y => $item) : ?>
								<div class="<?= ($y < 3) ? 'col-lg-4 col-12' : 'col-lg-6 col-12'; ?> content-item-col
								<?= !($item['i_img']) ? 'col-only-text' : ''; ?>">
									<?php if ($item['i_img']) : ?>
										<div class="image-item">
											<img src="<?= $item['i_img']['url']; ?>" alt="technology-image">
										</div>
									<?php endif; ?>
									<div class="base-text-item">
										<?= $item['i_text']; ?>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
	endif; ?>
</section>
<?php if ($fields['tech_video_file_3']) : ?>
	<section class="video-home">
		<video id="video-home">
			<source src="<?= $fields['tech_video_file_3']['url']; ?>" type="video/mp4">
		</video>
		<span id="video-button" class="button-home">
			<img src="<?= ICONS ?>play.png" alt="play-video">
		</span>
	</section>
<?php endif;
get_footer(); ?>
