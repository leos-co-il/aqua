<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
if ($fields['main_slider']) : ?>
	<section class="home-main-block">
		<div class="main-slider" dir="rtl">
			<?php foreach ($fields['main_slider'] as $slide) : ?>
				<div class="slide-home" <?php if ($slide['sl_image']): ?>
					style="background-image: url('<?= $slide['sl_image']['url']; ?>')"
				<?php endif; ?>>
					<div class="main-slide-item">
						<div class="container">
							<div class="row justify-content-center">
								<div class="col-auto d-flex flex-column align-items-center">
									<?php if ($slide['sl_title']) : ?>
										<h2 class="home-main-title">
											<?= $slide['sl_title']; ?>
										</h2>
									<?php endif;
									if ($slide['sl_link']) :  ?>
										<a href="<?= $slide['sl_link']['url']; ?>" class="base-link bigger-main-link">
											<?= (isset($slide['sl_link']['title']) && $slide['sl_link']['title']) ?
													$slide['sl_link']['title'] : 'לקטלוג 2021'; ?>
										</a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif; ?>
<section class="home-all-cats-block">
	<div class="cats-block-home">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title"><?= $fields['home_cats_title'] ?? 'AQUA MARINA 2021'; ?></h2>
				</div>
			</div>
			<?php if ($fields['home_cats_main']) : ?>
				<div class="row align-items-stretch justify-content-between">
					<?php foreach ($fields['home_cats_main'] as $x => $cat_link) : ?>
						<div class="col-md-auto col-cat-main wow fadeInUp" data-wow-delay="0.<?= $x * 2; ?>s">
							<a class="base-link bigger-link" href="<?= get_term_link($cat_link); ?>">
								<?= $cat_link->name; ?>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="home-cats-images-block">
		<?php if ($fields['home_cats_back']) : ?>
			<img src="<?= $fields['home_cats_back']['url']; ?>" alt="serfs-image" class="cats-back-image">
		<?php endif;
		if ($fields['home_cats_images']) : ?>
		<div class="container cats-container">
			<div class="row">
				<div class="col-12">
					<div class="row cats-col-line">
						<?php foreach ($fields['home_cats_images'] as $y =>  $serf_cat) : ?>
							<div class="col-xl-auto col-md-3 col-sm-4 col-6 cat-item-wrap wow fadeInUp"
							data-wow-delay="0.<?= $y; ?>s">
								<a class="cat-item" href="<?= get_term_link($serf_cat); ?>">
									<?php $thumbnail_id = get_term_meta( $serf_cat->term_id, 'thumbnail_id', true );
									$image = wp_get_attachment_url( $thumbnail_id );
									if ($image) : ?>
										<div class="cat-img-wrap">
											<img src="<?= $image; ?>" alt="category-image">
										</div>
									<?php endif; ?>
									<h3 class="base-text mb-1"><?= $serf_cat->name; ?></h3>
									<?php if ($name_en = get_field('cat_en', $serf_cat)) : ?>
										<h3 class="base-text mb-1"><?= $name_en; ?></h3>
									<?php endif; ?>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>
<?php if ($fields['home_slider_gallery']) : ?>
	<div class="gallery-all">
		<div class="gallery-slider" dir="rtl">
			<?php foreach ($fields['home_slider_gallery'] as $img) : ?>
				<a class="gallery-item" href="<?= $img['url']; ?>" style="background-image:
						url('<?= $img['url']; ?>')" data-lightbox="image">
				</a>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif;
if ($fields['home_review_item']) : ?>
	<section class="margin-50 reviews-block">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title"><?= $fields['home_review_title'] ?? 'ממליצים עלינו'; ?></h2>
				</div>
			</div>
			<?php if ($fields['home_review_item']) : ?>
				<div class="row">
					<div class="col-12 position-relative">
						<h3 class="quotes quote-top">&#8221;</h3>
						<h3 class="quotes quote-bottom">&#8220;</h3>
						<div class="reviews-wrapper">
							<div class="reviews-slider" dir="rtl">
								<?php foreach ($fields['home_review_item'] as $review) : ?>
									<div>
										<div class="row justify-content-center align-items-start">
											<?php if ($review['r_image']) : ?>
												<div class="col-lg-6 mb-lg-0 mb-4">
													<img src="<?= $review['r_image']['url']; ?>" alt="review-author">
												</div>
											<?php endif; ?>
											<div class="<?= $review['r_image'] ? 'col-lg-6 col-12' : 'col-12'; ?>">
												<div class="review-col-content">
													<h3 class="review-name"><?= $review['r_author']; ?></h3>
													<h4 class="review-subtitle"><?= $review['r_theme']; ?></h4>
													<div class="base-small-output">
														<?= $review['r_text']; ?>
													</div>
													<div class="socials-line">
														<?php if ($review['r_youtube']) : ?>
															<a href="<?= $review['r_youtube']; ?>" class="social-link">
																<img src="<?= ICONS ?>youtube.png" alt="youtube">
															</a>
														<?php endif;
														if ($review['r_instagram']) : ?>
															<a href="<?= $review['r_instagram']; ?>" class="social-link">
																<img src="<?= ICONS ?>instagram.png" alt="instagram">
															</a>
														<?php endif; ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endif;
			if ($fields['home_reviews_link']) :  ?>
			<div class="row justify-content-center mt-4">
				<div class="col-auto">
					<a href="<?= $fields['home_reviews_link']['url']; ?>" class="base-link">
						<?= (isset($fields['home_reviews_link']['title']) && $fields['home_reviews_link']['title']) ?
								$fields['home_reviews_link']['title'] : 'לכל ההמלצות'; ?>
					</a>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['home_posts']) : ?>
	<section class="posts-output margin-50">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title"><?= $fields['home_posts_title'] ?? 'המדיה שלנו'; ?></h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($fields['home_posts'] as $x => $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]);
				} ?>
			</div>
			<?php if ($fields['home_posts_link']) :  ?>
				<div class="row justify-content-center mt-4">
					<div class="col-auto">
						<a href="<?= $fields['home_posts_link']['url']; ?>" class="base-link">
							<?= (isset($fields['home_posts_link']['title']) && $fields['home_posts_link']['title']) ?
									$fields['home_posts_link']['title'] : 'לכל המדיה'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
	<?php get_template_part('views/partials/repeat', 'video_modal');
	endif;
if ($fields['home_video_file']) : ?>
	<section class="video-home">
		<video id="video-home">
			<source src="<?= $fields['home_video_file']['url']; ?>" type="video/mp4">
		</video>
		<span id="video-button" class="button-home">
			<img src="<?= ICONS ?>play.png" alt="play-video">
		</span>
	</section>
<?php endif;
if ($fields['community_item']) : ?>
	<section class="community-block margin-50">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title"><?= $fields['home_community_title'] ?? 'הקהילה שלנו'; ?></h2>
				</div>
			</div>
		</div>
		<div class="member-part-line">
			<?php foreach ($fields['community_item'] as $num => $member) : ?>
				<div class="member-part-col">
					<?php get_template_part('views/partials/card', 'member', [
							'item' => $member,
					]); ?>
				</div>
			<?php endforeach; ?>
		</div>
		<?php if ($fields['home_community_link']) :  ?>
			<div class="container">
				<div class="row justify-content-center mt-4">
					<div class="col-auto">
						<a href="<?= $fields['home_community_link']['url']; ?>" class="base-link">
							<?= (isset($fields['home_community_link']['title']) && $fields['home_community_link']['title']) ?
									$fields['home_community_link']['title'] : 'עוד על הקהילה'; ?>
						</a>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'instagram');
if ($fields['home_gallery']) {
	get_template_part('views/partials/content', 'gallery', [
			'gallery' => $fields['home_gallery'],
	]);
}
get_footer(); ?>
