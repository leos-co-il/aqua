<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');


?>
<article class="contact-page-body" <?php if (has_post_thumbnail()) : ?>
	style="background-image: url('<?= postThumb(); ?>')"
<?php endif; ?>>
	<?php if ($fields['contact_img_1']) : ?>
		<div class="serfer-img serf-1">
			<img src="<?= $fields['contact_img_1']['url']; ?>" alt="serfer-1">
		</div>
	<?php endif;
	if ($fields['contact_img_2']) : ?>
		<div class="serfer-img serf-2">
			<img src="<?= $fields['contact_img_2']['url']; ?>" alt="serfer-2">
		</div>
	<?php endif; ?>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-7 col-lg-8 col-md-9 col-sm-11 col-12">
				<?php if ($fields['contact_subtitle']) : ?>
					<h2 class="block-title"><?= $fields['contact_subtitle']; ?></h2>
				<?php endif; ?>
					<h1 class="home-main-title"><?php the_title(); ?></h1>
			</div>
			<div class="col-xl-7 col-lg-8 col-md-9 col-sm-11 col-12">
				<?php getForm('6'); ?>
			</div>
		</div>
	</div>
</article>

<?php get_footer(); ?>
