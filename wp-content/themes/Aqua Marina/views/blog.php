<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$posts_all = get_posts([
	'numberposts' => -1,
	'post_type' => 'post',
]);
$query_args = [
	'post_type' => 'post',
	'posts_per_page' => 12,
];
$posts = new WP_Query($query_args);
?>
<article class="page-body">
	<?php if (has_post_thumbnail()) : ?>
		<img src="<?= postThumb(); ?>" alt="page-image" class="w-100">
	<?php endif; ?>
	<div class="container-fluid my-4">
		<div class="row justify-content-center">
			<div class="col-11">
				<div class="row justify-content-center">
					<div class="col-auto">
						<h1 class="home-main-title"><?php the_title(); ?></h1>
						<div class="block-page-text">
							<?php the_content();  ?>
						</div>
					</div>
				</div>
				<?php if ($posts->have_posts()) : ?>
					<div class="row justify-content-center align-items-stretch put-here-posts">
						<?php foreach ($posts->posts as $x => $post) : ?>
							<?php get_template_part('views/partials/card', 'post', [
									'post' => $post,
							]); ?>
						<?php endforeach; ?>
					</div>
				<?php else: ?>
					<div class="row my-3">
						<div class="col-12">
							<h3 class="base-title text-center">
								<?= esc_html__('שום דבר לא נמצא','leos'); ?>
							</h3>
						</div>
					</div>
				<?php endif; ?>
				<?php if (count($posts_all) > 12 && $posts->have_posts()) : ?>
					<div class="row justify-content-center">
						<div class="col-auto">
							<div class="base-link bigger-link load-more-link load-more-posts" data-type="post">
								עוד מאמרים
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'video_modal');
get_footer(); ?>
