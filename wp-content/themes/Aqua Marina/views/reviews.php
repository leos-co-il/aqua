<?php
/*
Template Name: ממליצים
*/

get_header();
$fields = get_fields();
?>


<article class="page-body page-id" data-id="<?= get_the_ID(); ?>">
	<?php if (has_post_thumbnail()) : ?>
		<img src="<?= postThumb(); ?>" alt="page-image" class="w-100">
	<?php endif; ?>
	<div class="container-fluid my-4" id="members-block">
		<div class="row justify-content-center">
			<div class="col-11">
				<div class="row justify-content-center">
					<div class="col-auto">
						<h1 class="home-main-title"><?php the_title(); ?></h1>
						<div class="base-output block-page-text">
							<?php the_content();  ?>
						</div>
					</div>
				</div>
				<?php if ($fields['review_item']) : ?>
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($fields['review_item'] as $num => $rev) : ?>
							<div class="col-lg-4 col-md-6 col-sm-9 col-12 mb-4 card-member-col <?= ($num < 9) ? 'show-col' : ''; ?>">
								<?php get_template_part('views/partials/card', 'member', [
										'item' => $rev,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif;
				if (count($fields['review_item']) > 9) : ?>
					<div class="row justify-content-center mt-4">
						<div class="col-auto">
							<div class="base-link bigger-link load-more-link" id="load-more-items-members">
								עוד המלצות
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'instagram');
if ($fields['review_page_video']) : ?>
	<section class="video-home">
		<video id="video-home">
			<source src="<?= $fields['review_page_video']['url']; ?>" type="video/mp4">
		</video>
		<span id="video-button" class="button-home">
			<img src="<?= ICONS ?>play.png" alt="play-video">
		</span>
	</section>
<?php endif;
get_footer(); ?>
