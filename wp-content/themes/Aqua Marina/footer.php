<?php
$tel_eilat = opt('tel_eilat');
$address_eilat = opt('address_eilat');
$tel_center = opt('tel_center');
$address_center = opt('address_center'); ?>
<footer>
	<div class="footer-main">
		<a id="go-top">
			<img src="<?= ICONS ?>to-top.png" alt="to-top">
			<h4 class="to-top-text text-center">למעלה</h4>
		</a>
		<div class="container">
			<div class="row justify-content-between">
				<?php if ($logo = opt('foo_logo')) : ?>
					<div class="col-lg col-sm-4 col-6 foo-menu">
						<a href="/" class="logo">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					</div>
				<?php endif;
				if ($menus = opt('foo_menu')) : foreach ($menus as $menu) : ?>
					<div class="col-lg col-sm-4 col-6 foo-menu main-footer-menu">
						<h3 class="foo-title"><?= $menu['menu_title']; ?></h3>
						<div class="menu-border-top">
							<ul>
								<?php foreach ($menu['menu_items'] as $link) : if (isset($link['menu_link']) && $link['menu_link']) : ?>
									<li>
										<a href="<?= $link['menu_link']['url']; ?>">
											<?= (isset($link['menu_link']['title']) && $link['menu_link']['title']) ?
													$link['menu_link']['title'] : ''; ?>
										</a>
									</li>
								<?php endif; endforeach; ?>
							</ul>
						</div>
					</div>
				<?php endforeach; endif; ?>
				<div class="col-lg-auto col-sm-4 col-6 foo-menu contacts-footer-menu">
					<h3 class="foo-title">צור קשר</h3>
					<div class="menu-border-top">
						<ul class="contact-foo-list">
							<?php if ($tel_eilat) : ?>
								<li>
									<a href="tel:<?= $tel_eilat; ?>" class="contact-info">
										<?= $tel_eilat.' אילת'; ?>
									</a>
								</li>
							<?php endif;
							if ($tel_center) : ?>
								<li>
									<a href="tel:<?= $tel_center; ?>" class="contact-info">
										<?= $tel_center.' מרכז'; ?>
									</a>
								</li>
							<?php endif;
							if ($mail = opt('mail')) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="contact-info">
										<?= 'מייל: '.$mail; ?>
									</a>
								</li>
							<?php endif;
							if ($address_eilat) : ?>
								<li>
									<a href="https://www.waze.com/ul?q=<?= $address_eilat; ?>" class="contact-info">
										<img src="<?= ICONS ?>foo-loc.png" alt="location-icon" class="loc-icon">
										<?= 'מיקום אילת: '.$address_eilat; ?>
									</a>
								</li>
							<?php endif;
							if ($address_center) : ?>
								<li>
									<a href="https://www.waze.com/ul?q=<?= $address_center; ?>" class="contact-info">
										<img src="<?= ICONS ?>foo-loc.png" alt="location-icon" class="loc-icon">
										<?= 'מיקום מרכז: '.$address_center; ?>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="row justify-content-center foo-socials">
				<div class="col-auto">
					<?php get_template_part('views/partials/repeat', 'socials'); ?>
				</div>
			</div>
			<?php if ($foo_links = opt('links_footer')) : ?>
				<div class="row justify-content-center">
					<div class="col-auto d-flex align-items-center flex-wrap mt-2">
						<?php foreach ($foo_links as $link_item) : ?>
							<a href="<?= $link_item['url']; ?>" class="foo-link-item">
								<?= $link_item['title'].' | '; ?>
							</a>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
