<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif;
$tel_eilat = opt('tel_eilat');
$address_eilat = opt('address_eilat');
$tel_center = opt('tel_center');
$address_center = opt('address_center');
?>


<header>
	<div class="drop-container">
	</div>
    <div class="container-fluid">
        <div class="row align-items-center">
			<div class="col header-col-links pr-xl-0 pr-4">
				<nav id="desktopNav" class="h-100 ml-xl-3 ml-0">
					<?php getMenu('header-menu', '3', '', 'h-100'); ?>
				</nav>
				<nav id="MainNav" class="h-100 ml-xl-3 ml-0">
					<div id="MobNavBtn">
						<span></span>
						<span></span>
						<span></span>
					</div>
					<?php getMenu('header-mobile-menu', '2', '', 'main_menu h-100'); ?>
				</nav>
				<a href="<?= wp_registration_url(); ?>" class="header-btn register-link">
					<img src="<?= ICONS ?>user.png">
				</a>
				<a href="<?= wc_get_cart_url(); ?>" class="header-btn">
					<img src="<?= ICONS ?>basket.png" alt="cart">
					<span class="count-circle" id="cart-count">
						<?= WC()->cart->get_cart_contents_count(); ?>
					</span>
				</a>
				<div class="search-trigger header-btn">
					<img src="<?= ICONS ?>search.png" alt="search-trigger">
				</div>
			</div>
			<?php if ($logo = opt('logo')) : ?>
			<div class="col-xl-2 col-lg-3 col-6">
				<a href="/" class="logo">
					<img src="<?= $logo['url'] ?>" alt="logo">
				</a>
            </div>
			<?php endif; ?>
			<div class="col d-flex justify-content-end align-items-center tels-col-header">
				<div class="header-socials">
					<?php get_template_part('views/partials/repeat', 'socials'); ?>
				</div>
				<?php if ($tel_eilat) : ?>
					<div class="tel-col">
						<span class="tel-title">אילת</span>
						<a href="tel:<?= $tel_eilat; ?>" class="tel-link">
							<span class="tel-number"><?= $tel_eilat; ?></span>
							<img src="<?= ICONS ?>header-tel.png" alt="tel-icon">
						</a>
					</div>
				<?php endif;
				if ($tel_center) : ?>
					<div class="tel-col">
						<span class="tel-title">מרכז</span>
						<a href="tel:<?= $tel_center; ?>" class="tel-link">
							<span class="tel-number"><?= $tel_center; ?></span>
							<img src="<?= ICONS ?>header-tel.png" alt="tel-icon">
						</a>
					</div>
				<?php endif; ?>
			</div>
        </div>
    </div>
</header>

<section class="pop-search">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-search">
					<span class="close-search">
							<img src="<?= ICONS ?>close-review.png">
						</span>
					<?php get_search_form(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
