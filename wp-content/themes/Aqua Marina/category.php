<?php
$query = get_queried_object();
get_header();
$posts = new WP_Query([
	'posts_per_page' => 12,
	'post_type' => 'post',
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	]
]);
$posts_all = get_posts([
	'numberposts' => -1,
	'post_type' => 'post',
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	]
]);
?>
<article class="page-body">
	<?php if ($img = get_field('shop_top_img', $query)) : ?>
		<img src="<?= $img['url']; ?>" alt="page-image" class="w-100">
	<?php endif; ?>
	<div class="container-fluid my-4">
		<div class="row justify-content-center">
			<div class="col-11">
				<div class="row justify-content-center">
					<div class="col-auto">
						<h1 class="home-main-title"><?= $query->name; ?></h1>
						<div class="block-page-text">
							<?= category_description();  ?>
						</div>
					</div>
				</div>
				<?php if ($posts->have_posts()) : ?>
					<div class="row justify-content-center align-items-stretch put-here-posts">
						<?php foreach ($posts->posts as $x => $post) : ?>
							<?php get_template_part('views/partials/card', 'post', [
									'post' => $post,
							]); ?>
						<?php endforeach; ?>
					</div>
				<?php else: ?>
					<div class="row my-3">
						<div class="col-12">
							<h3 class="block-title text-center">
								<?= esc_html__('שום דבר לא נמצא','leos'); ?>
							</h3>
						</div>
					</div>
				<?php endif;
				if (count($posts_all) > 12 && $posts->have_posts()) : ?>
					<div class="row justify-content-center">
						<div class="col-auto">
							<div class="base-link bigger-link load-more-link load-more-posts" data-type="post"
								 data-term="<?= $query->term_id; ?>" data-tax-type="category">עוד מאמרים</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
