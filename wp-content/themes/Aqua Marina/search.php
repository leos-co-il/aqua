<?php
get_header();
$fields = get_fields();
?>
<div class="post-output-block mb-5">
	<div class="container pt-5">
		<?php
		$s = get_search_query();
		$args_1 = array(
			'post_type' => 'post',
			's' => $s
		);
		$args_2 = array(
			'post_type' => 'product',
			's' => $s
		);
		$the_query_1 = new WP_Query( $args_1 );
		$the_query_2 = new WP_Query( $args_2 );
		if ( $the_query_1->have_posts() ) { ?>
		<h4 class="base-title-red my-3"><?= esc_html__('תוצאות חיפוש עבור:','leos');?><?= get_query_var('s') ?></h4>
		<div class="row justify-content-center align-items-stretch">
			<?php while ( $the_query_1->have_posts() ) { $the_query_1->the_post();
				$link = get_the_permalink();
					$video = get_field('post_video', get_the_ID());
					$base_img = has_post_thumbnail() ? postThumb() : '';
					$image = $video ? getYoutubeThumb($video) : $base_img; ?>
					<div class="col-lg-3 col-md-6 col-sm-10 col-12 post-item-col">
						<div class="post-item more-card">
							<?php if ($video) : ?>
								<div class="post-item-img" <?php if ($image) : ?>
									style="background-image: url('<?= $image; ?>')" <?php endif; ?>>
									<span class="play-button" data-video="<?= getYoutubeId($video); ?>">
										<img src="<?= ICONS ?>play.png" alt="play-video">
									</span>
								</div>
							<?php else: ?>
								<a class="post-item-img" <?php if (has_post_thumbnail()) : ?>
									style="background-image: url('<?= postThumb(); ?>')"
								<?php endif; ?> href="<?= $link; ?>">
								</a>
							<?php endif; ?>
							<div class="home-item-content">
								<div class="flex-grow-1 d-flex justify-content-center">
									<a class="post-item-title" href="<?= $link; ?>"><?php the_title(); ?></a>
								</div>
								<p class="base-text text-center">
									<?= text_preview(get_the_content(), 14); ?>
								</p>
							</div>
						</div>
					</div>
			<?php }
			} else{ ?>
				<div class="col-12 pt-5">
					<h4 class="block-title">
						<?= esc_html__('שום דבר לא נמצא','leos'); ?>
					</h4>
				</div>
				<div class="alert alert-info text-center mt-5">
					<p><?= esc_html__('מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.','leos'); ?></p>
				</div>
			<?php } ?>
		</div>
		<div class="row justify-content-center align-items-start">
		<?php if ( $the_query_2->have_posts() ) {
			while ( $the_query_2->have_posts() ) { $the_query_2->the_post(); ?>
				<div class="col-lg-3 col-md-6 col-sm-10 col-12 product-card-col">
					<?php
					$post_object = get_post( get_the_ID());

					setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

					wc_get_template_part( 'content', 'product' );
					?>
				</div>
			<?php }
			} else { ?>
				<div class="col-12 pt-5">
					<h4 class="block-title">
						<?= esc_html__('שום דבר לא נמצא','leos'); ?>
					</h4>
				</div>
				<div class="alert alert-info text-center mt-5">
					<p><?= esc_html__('מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.','leos'); ?></p>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
