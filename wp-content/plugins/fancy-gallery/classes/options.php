<?php

namespace WordPress\Plugin\GalleryManager;

abstract class Options
{
    const
        page_slug = 'gallery-options', # The options page slug
        options_key = 'gallery_options'; # the field identifier in the options table

    private static
        $arr_option_box; # Meta boxes for the option page

    public static function init()
    {
        # Option boxes
        static::$arr_option_box = [
            'main' => [],
            'side' => []
        ];

        add_Action('admin_menu', [static::class, 'addOptionsPage']);
    }

    public static function addOptionsPage()
    {
        $handle = add_Options_Page(
            I18n::__('Gallery Options'),
            I18n::__('Galleries'),
            'manage_options',
            static::page_slug,
            [static::class, 'printOptionsPage']
        );

        # Add options page link to post type sub menu
        add_SubMenu_Page('edit.php?post_type=' . Post_Type::post_type_name, Null, I18n::__('Settings'), 'manage_options', 'options-general.php?page=' . static::page_slug);

        # Add JavaScript to this handle
        add_Action('load-' . $handle, [static::class, 'loadOptionsPage']);

        # Add option boxes
        static::addOptionBox(I18n::__('Gallery Management'), Core::$plugin_folder . '/options-page/gallery-management.php');
        static::addOptionBox(I18n::__('Lightbox'), Core::$plugin_folder . '/options-page/lightbox.php');
        static::addOptionBox(I18n::__('Gallery Previews'), Core::$plugin_folder . '/options-page/previews.php', 'main');
        static::addOptionBox(I18n::__('Taxonomies'), Core::$plugin_folder . '/options-page/taxonomies.php', 'side');
        static::addOptionBox(I18n::__('Gallery Archive'), Core::$plugin_folder . '/options-page/archive.php', 'side');
    }

    public static function getOptionsPageUrl($parameters = [])
    {
        $url = add_Query_Arg(['page' => static::page_slug], Admin_Url('options-general.php'));
        if (is_Array($parameters) && !empty($parameters)) $url = add_Query_Arg($parameters, $url);
        return $url;
    }

    public static function loadOptionsPage()
    {
        # If the Request was redirected from a "Save Options"-Post
        if (isset($_REQUEST['options_saved'])) {
            Taxonomies::updateTaxonomyNames();
            Post_Type::updatePostTypeName();
            flush_Rewrite_Rules();
        }

        # If this is a Post request to save the options
        if (static::saveOptions()) {
            WP_Redirect(static::getOptionsPageUrl(['options_saved' => 'true']));
        }

        WP_Enqueue_Style('dashboard');

        WP_Enqueue_Script(static::page_slug, Core::$base_url . '/options-page/options-page.js', ['jquery'], Core::version, True);
        WP_Enqueue_Style(static::page_slug, Core::$base_url . '/options-page/options-page.css');

        # Remove incompatible JS Libs
        WP_Dequeue_Script('post');
    }

    public static function printOptionsPage()
    {
        ?>
        <div class="wrap">
            <h2><?php I18n::_e('Gallery Settings') ?></h2>

            <?php if (isset($_GET['options_saved'])) : ?>
                <div id="message" class="updated fade">
                    <p><strong><?php I18n::_e('Settings saved.') ?></strong></p>
                </div>
            <?php endif ?>

            <form method="post" action="<?php echo remove_Query_Arg('options_saved') ?>">
                <div class="metabox-holder">

                    <div class="postbox-container left">
                        <?php foreach (static::$arr_option_box['main'] as $box) : ?>
                            <div class="postbox">
                                <div class="postbox-header">
                                    <h2 class="hndle"><?php echo $box->title ?></h2>
                                </div>
                                <div class="inside"><?php include $box->file ?></div>
                            </div>
                        <?php endforeach ?>
                    </div>

                    <div class="postbox-container right">
                        <?php foreach (static::$arr_option_box['side'] as $box) : ?>
                            <div class="postbox">
                                <div class="postbox-header">
                                    <h2 class="hndle"><?php echo $box->title ?></h2>
                                </div>
                                <div class="inside"><?php include $box->file ?></div>
                            </div>
                        <?php endforeach ?>
                    </div>

                </div>

                <p class="submit">
                    <input type="submit" class="button-primary" value="<?php I18n::_e('Save Changes') ?>">
                </p>

                <?php WP_Nonce_Field('save_gallery_manager_options') ?>
            </form>
        </div>
        <?php
    }

    public static function addOptionBox($title, $include_file, $column = 'main')
    {
        # Check the input
        if (!is_File($include_file)) return False;
        if (empty($title)) $title = '&nbsp;';

        # Column (can be 'side' or 'main')
        $column = ($column != 'main') ? 'side' : 'main';

        # Add a new box
        static::$arr_option_box[$column][] = (object) ['title' => $title, 'file' => $include_file];
    }

    public static function saveOptions(): bool
    {
        # Check if this is a post request
        if (empty($_POST)) return False;

        # Check the nonce
        check_Admin_Referer('save_gallery_manager_options');

        # Add Capabilities
        if (isset($_POST['capabilities']) && is_Array($_POST['capabilities'])) {
            foreach ($_POST['capabilities'] as $role_name => $arr_role) {
                if (!$role = get_Role($role_name)) continue;
                setType($arr_role, 'ARRAY');
                foreach ($arr_role as $capability => $yes_no) {
                    if ($yes_no == 'yes')
                        $role->add_Cap($capability);
                    else
                        $role->remove_Cap($capability);
                }
            }
            unset($_POST['capabilities']);
        }

        # Clean the Post array
        $options = stripSlashes_Deep($_POST);
        unset($options['_wpnonce'], $options['_wp_http_referer']);
        $options = Array_Filter($options, function ($value) {
            return $value == '0' || !empty($value);
        });

        # Save Options
        delete_Option('WordPress\Plugin\Fancy_Gallery\Options');
        delete_Option('wp_plugin_fancy_gallery_pro');
        delete_Option('wp_plugin_fancy_gallery');
        return (bool) update_Option(static::options_key, $options);
    }

    public static function getDefaultOptions()
    {
        return [
            'enable_editor' => False,
            'enable_block_editor' => False,
            'enable_featured_image' => True,
            'enable_custom_fields' => False,

            'lightbox' => True,
            'continuous' => False,
            'title_description' => True,
            'close_button' => True,
            'indicator_thumbnails' => True,
            'slideshow_button' => True,
            'slideshow_speed' => 3000, # Slideshow speed in milliseconds
            'preload_images' => 3,
            'animation_speed' => 400,
            'stretch_images' => False,
            'script_position' => 'footer',

            'gallery_taxonomy' => [],

            'enable_previews' => True,
            'enable_previews_for_custom_excerpts' => False,
            'preview_thumb_size' => 'thumbnail',
            'preview_columns' => 3,
            'preview_image_number' => 3,

            'enable_archive' => True,
        ];
    }

    public static function get($key = Null, $default = False)
    {
        static $arr_options;

        if (empty($arr_options)) {
            # Read Options
            $arr_options = Array_Merge(
                static::getDefaultOptions(),
                (array) get_Option('wp_plugin_fancy_gallery_pro'),
                (array) get_Option('wp_plugin_fancy_gallery'),
                (array) get_Option('WordPress\Plugin\Fancy_Gallery\Options'),
                (array) get_Option(static::options_key)
            );
        }

        # Locate the option
        if (empty($key))
            return $arr_options;
        elseif (isset($arr_options[$key]))
            return $arr_options[$key];
        else
            return $default;
    }
}

Options::init();
