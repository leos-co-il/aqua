<?php

// Configuration common to all environments
include_once __DIR__ . '/wp-config.common.php';

define("WP_DEBUG", true);
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define('DB_NAME', 'aqua-il');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '2208');
define('WP_MEMORY_LIMIT', '128M');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CT6$l6.0xOd#6dHson.Jxs`5GW]SULx8$/Oa7@.j~(QhMrHosrFhE&P<4B?$6K9Q');
define('SECURE_AUTH_KEY',  '{m!,4sck:]|n.[-fs]WxE#X-Q@=wV%m*kb:gw.4c6v{-)6UZu1pQd~cu+R&uME3n');
define('LOGGED_IN_KEY',    'Qiby#%0Me. :*+#qv4TbQ)J@bZB}meBlfH|jYKAp6E7=bH D}lDgc1q-6(n+Iw5w');
define('NONCE_KEY',        '#ywm!Nqe(M|&kGZB2-,Q{%}hSPB|e;} Bq/Rv22?pG$y3CPpAo<5z469n $uvZ$c');
define('AUTH_SALT',        'QR3c+-C1Y+DfOTx &>Jl&2Sv0EF*tct0)^c h!GU|yYhpMDKt[ne,1=UhKq~^;!Q');
define('SECURE_AUTH_SALT', 'I97|$$YFA*|V HE#ym|:rS>!rXwG8oT:j)W+z{Ylb1i#iC+|l34N9{~93&USMt#v');
define('LOGGED_IN_SALT',   'Jqa[cf/qyk?I7)x^v!59d|0e}Gk}Dkc1 Smdk!NJxA*S/0,1B@1*eukqk#2X7<i`');
define('NONCE_SALT',       'k%dUOXYRiN&M~+# v1zhY*~rd<>kad^S.D3D=nctk=Ez#F_WxVixpF-r*D!r?R4l');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'fmn_';

define('VP_ENVIRONMENT', 'default');
/* That's all, stop editing! Happy blogging. */
/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
